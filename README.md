# README #

## Narwhal project ##

The Narwhal project provides a finite element solver for registration problems formulated as a flow equation (PDE) evolving on a manifold. The implementation is done via the Python interface of the FEniCS finite element solver.

References:

 * [https://en.wikipedia.org/wiki/Computational_anatomy](https://en.wikipedia.org/wiki/Computational_anatomy)
 * [http://dx.doi.org/10.1137/151006238](http://dx.doi.org/10.1137/151006238)
 * [https://fenicsproject.org/](FEniCS Project)

## How do I get set up? ##

1. Install [FEniCS](https://fenicsproject.org/), version 2017.1 e.g. using the Docker interface.
1. Make a Python program, e.g. by following the example below.

### Example program ###

    if __name__ == "__main__":
        import narwhal as na

        # General
        targetimage     = 'face_target_50.png'
        timestepsize    = 1.0
        tol             = 0.00001
        maxsteps        = 500
		savestep        = 10
    
        # A parameter for how well the transformation preserves sharp corners of the image.
        # Integers between (and including) 1 and 4 are currently possible
        k               = 3
        # A parameter for how local/global the transformation is. 
        epsilon         = 1.0
    
        # Face 1
        sourceimage     = 'face_source1_50.png'
        outputdirectory = 'fixedmeshsolver_face1_50'

        na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon)           # Standard solver
        na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")     # LU solver
        na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "CGAMG")  # CG/AMG solver
		
## Contributions ##
**Jenny Larsson**  
Department of Animal and Plant Sciences  
University of Sheffield, Sheffield, UK

[Web page](http://www.sheffield.ac.uk/)

**Anders Logg**  
Department of Mathematics  
Chalmers University of Technology, Gothenburg, Sweden

[Web page](http://www.logg.org/anders/)

**Klas Modin**  
Department of Mathematics  
Chalmers University of Technology, Gothenburg, Sweden

[Web page](http://klasmodin.wordpress.com)

**Alexander Sehlstr�m**  
Architecture and Engineering Research Group  
Department of Architecture and Civil Engineering  
Chalmers University of Technology, Gothenburg, Sweden

[Web page](http://sehlstrom.se/)

## NSCM 30 ##
Code to generate the data for the examples presented at NSCM 30 and in proceeding short abstract "Moving mesh and image registration in FEniCS".

### Example 1: Proof of concept using smileys ###

    if __name__ == "__main__":
        import narwhal as na

        # General settings
		targetimage     = 'face_target_50.png'
		timestepsize    = 1.0
		tol             = 0.00001
		maxsteps        = 600
		savestep        = 10
		k               = 3
		epsilon         = 1.0
    
		# Face 1
		sourceimage     = 'face_source1_50.png'
		outputdirectory = 'fixedmeshsolver_face1_50'
    
		na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")
    
		# Face 2
		sourceimage     = 'face_source2_50.png'
		outputdirectory = 'fixedmeshsolver_face2_50'

		na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")
		
		# Face 3
		sourceimage     = 'face_source3_50.png'
		outputdirectory = 'fixedmeshsolver_face3_50'

		na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")
		
		# Face 4
		sourceimage     = 'face_source4_50.png'
		outputdirectory = 'fixedmeshsolver_face4_50'

		na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")
		
		# Face 5
		sourceimage     = 'face_source5_50.png'
		outputdirectory = 'fixedmeshsolver_face5_50'
		
		na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")
		
		# Face 6
		sourceimage     = 'face_source6_50.png'
		outputdirectory = 'fixedmeshsolver_face6_50'
		
		na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, "LU")


### Example 2: Testing the different values of smoothness parameter k for snail images ###

	if __name__ == "__main__":
	    import narwhal as na
	
	    # Parameters for snail images
	    targetimage     = 'shell1.png'
	    sourceimage     = 'shell5.png'
	    timestepsize    = 1.0
	    tol             = 0.00005
	    maxsteps        = 300 
	    epsilon         = 1.0
	    savestep        = 1
	
	    # Run program for different values of k.
	    k = 1 
	    outputdirectory  = 'output_CTsnails_k1'
	    na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon)
	    
	    k = 2
	    outputdirectory  = 'output_CTsnails_k2'
	    na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon)
    
	    k = 3
	    outputdirectory  = 'output_CTsnails_k3'
	    na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon)
	    
	    k = 4
	    outputdirectory  = 'output_CTsnails_k4'
	    na.fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon)
