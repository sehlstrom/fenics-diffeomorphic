# -*- coding: iso-8859-15 -*-

# Moving mesh and image registration using FEniCS.
#
# References:
# [1]   http://dx.doi.org/10.1137/151006238
#
# Code by:
#       Alexander Sehlstrom
#       alexander.sehlstrom@chalmers.se
#       Department of Architecture and Civil Engineering
#       Chalmers University of Technology
#
#       Jenny Larsson
#       jslarsson1@sheffield.ac.uk
#       Department of Animal and Plant Sciences
#       University of Sheffield
#
#       Klas Modin
#       klas.modin@chalmers.se
#       Department of Mathematical Sciences
#       Chalmers University of Technology
import numpy as np
import fenics as fe
import narwhal as na

fe.parameters["form_compiler"]["cpp_optimize"] = True
fe.parameters["form_compiler"]["representation"] = "uflacs"
fe.set_log_level(fe.WARNING)

# Generate a folder for output
mydir = 'moving_mesh_registration_output/'
na.ensureoutputfolder(mydir)

# Read the images to arrays and make values into the span [0,1]
from scipy import misc
I0array = misc.imread('face_source1_50.png', flatten=True, mode='F') / 255.0
I1array = misc.imread('face_target_50.png', flatten=True, mode='F') / 255.0

# Check dimensions of arrays
if I0array.shape != I1array.shape:
    raise NameError('Dimension mismatch of provided image arrays.')
Lx, Ly = I0array.shape

print("Image size %ix%i pixels" % (Lx, Ly))

# Make fixed and moving mesh
mesh_fixed = fe.RectangleMesh(fe.Point(0.5, 0.5), fe.Point(Lx-0.5, Ly-0.5), Lx-1, Ly-1, 'right')
mesh_moving = fe.RectangleMesh(fe.Point(0.5, 0.5), fe.Point(Lx-0.5, Ly-0.5), Lx-1, Ly-1, 'right')

# Function space for fixed images
V_fixed = fe.FunctionSpace(mesh_fixed, "CG", 1)

# Function space for moving images
V_moving = fe.FunctionSpace(mesh_moving, "CG", 1)

# Vector valued function space for fixed vector fields
W_fixed = fe.VectorFunctionSpace(mesh_fixed, "CG", 1)

# Vector valued function space for moving vector fields
W_moving = fe.VectorFunctionSpace(mesh_moving, "CG", 1)

# Generate image functions
I0 = na.arraytointensity(mesh_fixed, V_fixed, I0array)           # Source image
I1 = na.arraytointensity(mesh_fixed, V_fixed, I1array)           # Target image
I_fixed = na.arraytointensity(mesh_fixed, V_fixed, I0array)      # Time t image on fixed mesh
#fe.parameters['allow_extrapolation'] = True
I_moving = na.arraytointensity(mesh_moving, V_moving, I0array)   # Time t image on moving mesh
#fe.parameters['allow_extrapolation'] = False

# Generate output for source and target
file = fe.File(mydir+"source.pvd")
file << I0
file = fe.File(mydir+"target.pvd")
file << I1

# Set time step
dt = fe.Constant(5.0)

# Define Poisson problem
sigma = fe.Constant(1)
u = fe.TrialFunction(W_fixed)
v = fe.TestFunction(W_fixed)
a1 = fe.dot(u,v)*fe.dx 
a2 = 2*fe.inner(fe.grad(u), fe.grad(v))*fe.dx 
a3 = fe.dot(fe.div(fe.grad(u)), fe.div(fe.grad(v)))*fe.dx
a_Poisson = a1 + a2 +a3
# a_Poisson = fe.inner(u,v)*fe.dx+sigma*fe.inner(fe.grad(u),fe.grad(v))*fe.dx
L_Poisson = -fe.div(v*(I_fixed-I1))*I_fixed*fe.dx
def boundary(x, on_boundary):
    return on_boundary
bc_Poisson = fe.DirichletBC(W_fixed, fe.Constant((0,0)), boundary)

# Define vector field function
u = fe.Function(W_fixed)

# Define backward Euler problem
I = fe.TrialFunction(V_fixed)
alpha = fe.TestFunction(V_fixed)
a_Euler = I*alpha*fe.dx - dt*I*fe.div(alpha*u)*fe.dx
L_Euler = I_fixed*alpha*fe.dx

# Define image function
I = fe.Function(V_fixed)

# Output files
trace_file = fe.File(mydir+"trace.pvd")
vel_file = fe.File(mydir+"velocity.pvd")

# Time-stepping loop
maxsteps = 500
step = 0
tol = 0.005
use_fixed_formulation = True
converged = False
while not converged:
    # Save trace of previous step
    trace_file << I_moving
    
    # Solve Poisson problem
    fe.solve(a_Poisson == L_Poisson, u, bc_Poisson)
    
    # Save velocity output
    vel_file << u

    if use_fixed_formulation:
        # Solve backward Euler problem
        fe.solve(a_Euler == L_Euler, I)
        I_fixed.assign(I)
    
    # Compute infinitesimal displacement on mesh_moving
    displacement = fe.project(dt*u, W_moving)

    # Displace mesh
    fe.ALE.move(mesh_moving, displacement)

    if not use_fixed_formulation:
        I_fixed.assign(fe.interpolate(I_moving, V_fixed))
        
    step = step + 1
    
    if fe.norm(u) < tol:
        converged = True
        print("Converged in step %i with tolerance %f" % (step, tol))
    
    if step >= maxsteps:
        print("Maximum number of allowed steps (%d) reached." % (maxsteps))
        break

