# -*- coding: iso-8859-15 -*-

# Transient solver for moving mesh and image registration using FEniCS.
#
# References:
# [1]   http://dx.doi.org/10.1137/151006238
#
# Code by:
#       Alexander Sehlstrom
#       alexander.sehlstrom@chalmers.se
#       Department of Architecture and Civil Engineering
#       Chalmers University of Technology
#
#       Jenny Larsson
#       jslarsson1@sheffield.ac.uk
#       Department of Animal and Plant Sciences
#       University of Sheffield
from fenics import *
import narwhal as na

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
parameters['allow_extrapolation'] = True

set_log_level(WARNING)

# Generate a folder for output
mydir = 'moving_mesh_registration_output/'
na.ensureoutputfolder(mydir)
        
# Read the images to arrays and make values into the span [0,1]
from scipy import misc
array0 = misc.imread('face_source.png', flatten=True, mode='F') / 255.0
array1 = misc.imread('face_target.png', flatten=True, mode='F') / 255.0

# Check dimensions of arrays and make mesh
if array0.shape == array1.shape:
    Lx, Ly = array0.shape
    mesh0,   V0,   W0 = torusmesh(Lx, Ly)
    mesht,   Vt,   Wt = torusmesh(Lx, Ly)
    meshtm1, Vtm1, Wtm1 = torusmesh(Lx, Ly)
    mesh1,   V1,   W1 = torusmesh(Lx, Ly)
else:
    raise NameError('Dimension mismatch of provided image arrays.')
    
# Generate density functions
mu0   = na.arraytodensity(mesh0,   V0,   array0)   # Source density
mut   = na.arraytodensity(mesht,   Vt,   array0)   # Current time step density
mutm1 = na.arraytodensity(meshtm1, Vtm1, array0)   # Last time step density
mu1   = na.arraytodensity(mesh1,   V1,   array1)   # Target density

file = File(mydir+"source.pvd")
file << mu0
file = File(mydir+"target.pvd")
file << mu1

# Time step
dt = Constant(2.0)

# Define variational problem
u = TrialFunction(W1)
v = TestFunction(W1)

sigma = Constant(0.05)

# Initial conditions
mut   = interpolate(mu0, Vt)
mutm1 = interpolate(mu0, Vtm1)

# Source term.
# mu0 and mu1 lives on differnet meshes so we need
# to interpolate one to the other mesh. Here we
# interpolate the (undeformed) target to the (deformed)
# source.
mut_on_mesh1 = interpolate(mut,V1)
# f2 = (1 / sigma) * (mut - interpolate(mutm1, Vt)) / dt
#f = f1# + f2

# Bilinear form.
a1 = dot(u,v)*dx 
a2 = 2*inner(grad(u), grad(v))*dx 
a3 = dot(div(grad(u)), div(grad(v)))*dx
a = a1 + a2 +a3

# Linear functional.
# L = - f*div(v)*dx
# L = inner(v,(mut_on_mesh1-mu1)*grad(mut_on_mesh1))*dx

# Trace file
file = File(mydir+"trace.pvd")
file2 = File(mydir+"velocity.pvd")

# Solve all time steps
u = Function(W1)
u_on_mesht = interpolate(u,Wt)
steps = 200
for step in range(steps):    
    # Save trace of previous step
    file << mut
    
    # Solve
    L = -div(v*(mut_on_mesh1-mu1))*mut_on_mesh1*dx
    solve(a == L, u)

    # Save velocity output
    file2 << u

    # Compute displacement distance
    displace = project(dt*u, Wt)
    
    # Displace mesh
    meshtm1 = mesht
    ALE.move(mesht, displace)

    mut_on_mesh1 = interpolate(mut,V1)

