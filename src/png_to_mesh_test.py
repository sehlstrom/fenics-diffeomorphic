from scipy import misc
from narwhal import *

# Read the image to an array.
array0 = misc.imread('source.png', flatten=True, mode='F')
array1 = misc.imread('target.png', flatten=True, mode='F')

# Make a mesh and mesh function mu
mesh0, V0, mu0 = arraytomesh(array0)
mesh1, V1, mu1 = arraytomesh(array1)

# Save to pvd
file0 = File("density_source.pvd")
file0 << mu0
file1 = File("density_target.pvd")
file1 << mu1