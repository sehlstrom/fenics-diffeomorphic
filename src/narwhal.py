# -*- coding: iso-8859-15 -*-

# Python and FEniCS implementation for moving mesh and image registration (MMIM)
#
# References:
# [1]   http://dx.doi.org/10.1137/151006238
#
# Code by:
#       Alexander Sehlstrom
#       alexander.sehlstrom@chalmers.se
#       Department of Architecture and Civil Engineering
#       Chalmers University of Technology
#
#       Jenny Larsson
#       jslarsson1@sheffield.ac.uk
#       Department of Animal and Plant Sciences
#       University of Sheffield
#
#       Klas Modin
#       klas.modin@chalmers.se
#       Department of Mathematical Sciences
#       Chalmers University of Technology

import numpy as np
import fenics as fe
import warnings
import functools

def deprecated(func):
    """This is a decorator which can be used to mark functions as deprecated.
    It will result in a warning being emmitted when the function is used."""
    
    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.simplefilter('always', DeprecationWarning) #turn off filter 
        warnings.warn("Call to deprecated function {}.".format(func.__name__), category=DeprecationWarning, stacklevel=2)
        warnings.simplefilter('default', DeprecationWarning) #reset filter
        return func(*args, **kwargs)
        
    return new_func


def torusmesh(Lx, Ly):
    """Creates a torus mesh of given size along with periodic boundary conditions
    and corresponding function spaces.
    
    mesh, V, W = torusmesh(Lx, Ly)
    
    Output:
    mesh    mesh object
    V       Function space with periodic bcs, for mu
    W       Vector valued function space with periodic bcs, for u and v
    
       <------------- Lx ------------>
    
    ↑  •••••••••••••••••••••••••••••••
    |  •     •     •     •     •     •<== Pixel/Voxel
    |  •  ○--•--○--•--○--•--○--•--○  •
    |  •  |  • /|  • /|  • /|  • /|  •
    |  •  |  •/ |  •/ |  •/ |  •/ |<===== Mesh
    |  •••••••••••••••••••••••••••••••
    |  •  | /•  | /•  | /•  | /•  |  •
       •  |/ •  |/ •  |/ •  |/ •  |  •
    Ly •  ○--•--○--•--○--•--○--•--○<===== Vertex
       •  |  • /|  • /|  • /|  • /|  •
    |  •  |  •/ |  •/ |  •/ |  •/ |  •
    |  •••••••••••••••••••••••••••••••
    |  •  | /•  | /•  | /•  | /•  |  •
    |  •  |/ •  |/ •  |/ •  |/ •  |  •
    |  •  ○--•--○--•--○--•--○--•--○  •
    |  •     •     •     •     •     •
    ↓  •••••••••••••••••••••••••••••••
    
    """
    
    # Create mesh
    mesh = fe.RectangleMesh(fe.Point(0.5/Lx,0.5/Ly), fe.Point(1-0.5/Lx, 1-0.5/Ly), Lx-1, Ly-1, 'right')
    
    # Sub domain for Periodic boundary condition
    class PeriodicBoundary(fe.SubDomain):

        def inside(self, x, on_boundary):
            return on_boundary

        # Map right boundary to left boundary and top boundary to bottom boundary
        def map(self, x, y):
            if fe.near(x[0], 1) and fe.near(x[1], 1):
                y[0] = x[0] - 1
                y[1] = x[1] - 1
            elif fe.near(x[0], 1):
                y[0] = x[0] - 1
                y[1] = x[1]
            else:       # near(x[1], 1)
                y[0] = x[0]
                y[1] = x[1] - 1
                
    # Function space with periodic bcs, for mu
    V = fe.FunctionSpace(mesh, "Lagrange", 1, constrained_domain=PeriodicBoundary())
    
    # Vector valued function space with periodic bcs, for u and v
    W = fe.VectorFunctionSpace(mesh, "Lagrange", 1, constrained_domain=PeriodicBoundary())
    
    return mesh, V, W

def arraytointensity(mesh, functionspace, array, k):
    """Given a mesh and a function space, the array is transformed into a intensity
    function I on the mesh. The function mu describes the intensity at each vertex/
    voxel corresponding to the cell value of the array.
    
    If the array is in 2D, each element of the array is thought of as an pixel.
    If the array is in 3D, each element of the array is thought of as an voxel.
    
    2D case example:
    
    •••••••••••••••••••••••••••••••
    •     •     •     •     •     •<== Pixel/Voxel
    •  ○--•--○--•--○--•--○--•--○  •
    •  |  • /|  • /|  • /|  • /|  •
    •  |  •/ |  •/ |  •/ |  •/ |<===== Mesh
    •••••••••••••••••••••••••••••••
    •  | /•  | /•  | /•  | /•  |  •
    •  |/ •  |/ •  |/ •  |/ •  |  •
    •  ○--•--○--•--○--•--○--•--○<===== Vertex
    •  |  • /|  • /|  • /|  • /|  •
    •  |  •/ |  •/ |  •/ |  •/ |  •
    •••••••••••••••••••••••••••••••
    •  | /•  | /•  | /•  | /•  |  •
    •  |/ •  |/ •  |/ •  |/ •  |  •
    •  ○--•--○--•--○--•--○--•--○  •
    •     •     •     •     •     •
    •••••••••••••••••••••••••••••••
    """
    # Define intensity function
    I = fe.Function(functionspace)
    
    # Size or image
    Lx, Ly = array.shape
    
    # Array to store vertex values in
    vertex_values = np.zeros(mesh.num_vertices())

    # Loop through array
    if array.ndim == 2:
        for vertex in fe.vertices(mesh):
            i = int(np.trunc(Lx*vertex.x(0)))
            j = int(np.trunc(Ly*vertex.x(1)))
            vertex_values[vertex.index()] = array[i,j].item()
    elif array.ndim == 3:
        for vertex in fe.vertices(mesh):
            i = int(np.trunc(vertex.x(0)))
            j = int(np.trunc(vertex.x(1)))
            k = int(np.trunc(vertex.x(2)))
            vertex_values[vertex.index()] = array[i,j,k].item()

    # Assign vertex values at correct dofs of I
    I.vector()[:] = vertex_values[fe.dof_to_vertex_map(functionspace)]
    
    # Project to a P2 functionspace
    functionspace_Pk = fe.FunctionSpace(mesh, "CG", k)
    
    I_Pk = fe.project(I, functionspace_Pk)
    
    return I_Pk

def ensureoutputfolder(mydir):
    """ If not existing, a output folder will be generated for output of data. """
    
    if not isinstance(mydir, str):
        raise ValueError("Parameter 'mydir' has to be a string")
        
    if not mydir.endswith('/'):
        mydir += str('/')
    
    import os, errno
    outputpath = os.path.join(os.getcwd(), mydir)
    if not os.path.exists(outputpath):
        try:
            os.makedirs(outputpath)
        except OSError as e:
            if e.errno != errno.EEXIST:
                # This was not a "directory exist" error; some other error occured
                raise  OSError("Problem with the output folder (%s)!" % (outputpath))
                
    return outputpath

def fixedmeshsolver(sourceimage, targetimage, outputdirectory, timestepsize, tol, maxsteps, savestep, k, epsilon, solver_method="standard"):
    """Moving mesh image registration solver using a fixed mesh.
    INPUT:
        sourceimage     Path to source image
        
        targetimage     Path to target image
        
        outputdirectory Path to folder where resulting output should be put
        
        timestepsize    Time step size
        
        tol             Tolearance for convergecy check
        
        maxsteps        Maximum of allowed steps; aborts solver when reached
        
        savestep        Number of steps from one save to another; increase to decrease solver speed
        
        k               1, 2, 3 or 4; Poisson problem parameter, see documentation
        
        epsilon         Poisson problem parameter, see documentation
        
        solver_method   "standard" FEniCS standard solver (default)
                        "LU"       LU factorisation solver
                        "CGAMG"    conjugate gradient (CG) solver with algebraic multi-grid (AMG) preconditioning
    """
    fe.parameters["form_compiler"]["cpp_optimize"] = True
    fe.parameters["form_compiler"]["representation"] = "uflacs"
    fe.set_log_level(fe.WARNING)

    # Generate a folder for output
    outputpath = ensureoutputfolder(outputdirectory)

    # Read the images to arrays and make values into the span [0,1]
    from scipy import misc, special
    I0array = misc.imread(sourceimage, flatten=True, mode='F') / 255.0
    I1array = misc.imread(targetimage, flatten=True, mode='F') / 255.0

    # Check dimensions of arrays
    if I0array.shape != I1array.shape:
        raise NameError('Dimension mismatch of provided image arrays.')
    Lx, Ly = I0array.shape

    # Tell about image size
    print("Solving for source %s and target %s" % (sourceimage, targetimage))
    print("Image size %ix%i pixels" % (Lx, Ly))
    print("Output in folder %s" % (outputpath))

    # Make fixed and moving mesh
    mesh_fixed = fe.RectangleMesh(fe.Point(0.5/Lx, 0.5/Ly), fe.Point(1-0.5/Lx, 1-0.5/Ly), Lx-1, Ly-1, 'right')
    mesh_moving = fe.RectangleMesh(fe.Point(0.5/Lx, 0.5/Ly), fe.Point(1-0.5/Lx, 1-0.5/Ly), Lx-1, Ly-1, 'right')

    # Function space for fixed images
    # TODO: Use P2. Needs changes in the array to mesh mapping.
    V_fixed = fe.FunctionSpace(mesh_fixed, "CG", 1)

    # Function space for moving images
    # TODO: Use P2. Needs changes in the array to mesh mapping.
    V_moving = fe.FunctionSpace(mesh_moving, "CG", 1)

    # Vector valued function space for fixed vector fields
    # TODO: Use P2. Needs changes in the array to mesh mapping.
    W_fixed = fe.VectorFunctionSpace(mesh_fixed, "CG", 1)

    # Vector valued function space for moving vector fields
    # TODO: Use P2. Needs changes in the array to mesh mapping.
    W_moving = fe.VectorFunctionSpace(mesh_moving, "CG", 1)

    # Generate image functions
    I1       = arraytointensity(mesh_fixed, V_fixed, I1array, k)     # Target image
    I_fixed  = arraytointensity(mesh_fixed, V_fixed, I0array, k)     # Time t image on fixed mesh
    I_moving = arraytointensity(mesh_moving, V_moving, I0array, k)   # Time t image on moving mesh

    # Generate output for source and target
    #xdmffile_source = fe.XDMFFile(outputpath+"source.xdmf")
    #xdmffile_target = fe.XDMFFile(outputpath+"target.xdmf")
    #xdmffile_source.write(I_fixed, 0)
    #xdmffile_target.write(I1, 0)
    pvdfile_source = fe.File(outputpath+"source.pvd")
    pvdfile_target = fe.File(outputpath+"target.pvd")
    pvdfile_source << I_fixed
    pvdfile_target << I1

    # Set time step
    dt = fe.Constant(timestepsize)
    
    # Define functions for the variational formulation
    u = fe.TrialFunction(W_fixed)
    w = fe.TestFunction(W_fixed)

    # Define Poisson problem of order k
    a1 = fe.dot(u,w)*fe.dx
    a2 = special.binom(k,1)*epsilon*fe.inner(fe.grad(u), fe.grad(w))*fe.dx
    a_Poisson = a1 + a2
    
    if k > 1:
        a3 = special.binom(k,2)*epsilon**2*fe.dot(fe.div(fe.grad(u)), fe.div(fe.grad(w)))*fe.dx
        a_Poisson += a3
        if k > 2:
            a4 = special.binom(k,3)*epsilon**3*fe.inner(fe.grad(fe.div(fe.grad(u))), fe.grad(fe.div(fe.grad(w))))*fe.dx
            a_Poisson += a4
            if k > 3:
                a5 = special.binom(k,4)*epsilon**4*fe.dot(fe.div(fe.grad(fe.div(fe.grad(u)))), fe.div(fe.grad(fe.div(fe.grad(w)))))*fe.dx
                a_Poisson += a5

    # sigma = 0.001
    # NOTE: CellVolume SHOULD BE DIFFERENCE BETWEEN mesh_fixed AND mesh_moving. HOW?
    L_Poisson = -fe.div(w*(I_fixed-I1))*I_fixed*fe.dx #- sigma*fe.CellVolume(mesh_fixed)*fe.div(w)*fe.dx

    # Define boundary condition
    def boundary(x, on_boundary):
        return on_boundary
    bc_Poisson = fe.DirichletBC(W_fixed, fe.Constant((0,0)), boundary)

    # Setup solver for Poisson linear equation system
    if solver_method == "LU":
        print("LU solver")
        A = fe.assemble(a_Poisson)
        solver_Poisson = fe.LUSolver(A)
        solver_Poisson.parameters['reuse_factorization'] = True
        bc_Poisson.apply(A)
    elif solver_method == "CGAMG":
        print("CG/AMG solver")
        rhs_fake_Poisson = -fe.div(w*(I_fixed-I1))*I_fixed*fe.dx
        A, _ = fe.assemble_system(a_Poisson, rhs_fake_Poisson, bc_Poisson)
        solver_Poisson = fe.KrylovSolver(A, "cg", "hypre_amg")
        solver_Poisson.parameters["absolute_tolerance"] = 1E-8
        solver_Poisson.parameters["relative_tolerance"] = 1E-8
    else:
        print("Standard solver")
    
    # Define vector field function
    u = fe.Function(W_fixed)

    # Define backward Euler problem
    I = fe.TrialFunction(V_fixed)
    v = fe.TestFunction(V_fixed)
    a_Euler = I*v*fe.dx - dt*I*fe.div(v*u)*fe.dx
    L_Euler = I_fixed*v*fe.dx

    # Define intensity function
    I = fe.Function(V_fixed)

    # Output trace file
    #xdmffile_trace = fe.XDMFFile(outputpath+"trace.xdmf")
    pvdfile_trace = fe.File(outputpath+"trace.pvd")

    # Time-stepping loop
    step = 0
    converged = False
    while not converged:
        # Save trace of previous step
        if step % savestep == 0:
            #xdmffile_trace.write(I_moving, step)       # Not working
            pvdfile_trace << I_moving

        # Solve Poisson problem
        if solver_method == "LU" or solver_method == "CGAMG":
            b = fe.assemble(L_Poisson)
            bc_Poisson.apply(b)
            solver_Poisson.solve(u.vector(), b)
        else:
            fe.solve(a_Poisson == L_Poisson, u, bc_Poisson)

        # Solve backward Euler problem
        fe.solve(a_Euler == L_Euler, I)
        I_fixed.assign(I)

        # Compute infinitesimal displacement on mesh_moving
        displacement = fe.project(dt*u, W_moving)

        # Displace mesh
        fe.ALE.move(mesh_moving, displacement)

        # Count step
        step = step + 1

        # Convergence test
        if fe.norm(u) < tol:
            converged = True
            print("Converged in step %i with tolerance=%f, timestepsize=%f, k=%d, epsilon=%f" % (step, tol, timestepsize, k, epsilon))
            if step % savestep != 0:
                #xdmffile_trace.write(I_moving, step)
                pvdfile_trace << I_moving
            break

        # Maximum number of allowed step test
        if step >= maxsteps:
            print("Maximum number of allowed steps (%d) reached." % (maxsteps))
            if step % savestep != 0:
                #xdmffile_trace.write(I_moving, step)
                pvdfile_trace << I_moving
            break

    print('Solver done')

@deprecated
def arraytodensity(mesh, V, array):
    """Given a mesh and a function space, the given array is transformed into
    a density function mu. The function mu describes the density at each
    vertex corresponding to the cell value of the array.
    
    If the array is in 2D, each element of the array is thought of as an pixel.
    If the array is in 3D, each element of the array is thought of as an voxel.
    
    2D case example:
    
    •••••••••••••••••••••••••••••••
    •     •     •     •     •     •<== Pixel/Voxel
    •  ○--•--○--•--○--•--○--•--○  •
    •  |  • /|  • /|  • /|  • /|  •
    •  |  •/ |  •/ |  •/ |  •/ |<===== Mesh
    •••••••••••••••••••••••••••••••
    •  | /•  | /•  | /•  | /•  |  •
    •  |/ •  |/ •  |/ •  |/ •  |  •
    •  ○--•--○--•--○--•--○--•--○<===== Vertex
    •  |  • /|  • /|  • /|  • /|  •
    •  |  •/ |  •/ |  •/ |  •/ |  •
    •••••••••••••••••••••••••••••••
    •  | /•  | /•  | /•  | /•  |  •
    •  |/ •  |/ •  |/ •  |/ •  |  •
    •  ○--•--○--•--○--•--○--•--○  •
    •     •     •     •     •     •
    •••••••••••••••••••••••••••••••
    """
    # Define density function
    mu = Function(V)
    
    # Array to store vertex values in
    vertex_values = np.zeros(mesh.num_vertices())

    # Loop through array
    if array.ndim == 2:
        for vertex in vertices(mesh):
            i = int(np.trunc(vertex.x(0)))
            j = int(np.trunc(vertex.x(1)))
            vertex_values[vertex.index()] = array[i,j].item()
    elif array.ndim == 3:
        for vertex in vertices(mesh):
            i = int(np.trunc(vertex.x(0)))
            j = int(np.trunc(vertex.x(1)))
            k = int(np.trunc(vertex.x(2)))
            vertex_values[vertex.index()] = array[i,j,k].item()

    # Assign vertex values at correct dofs of mu
    mu.vector()[:] = vertex_values[dof_to_vertex_map(V)]
    
    return mu

@deprecated
def arraytomesh(array):
    """
    Takes an array and makes it into a FEniCS mesh.
    
    If the array is in 2D, each element of the array is thought of as an pixel.
    If the array is in 3D, each element of the array is thought of as an voxel.
    
    Returns a FEniCS mesh, the function space and a mesh function describing the
    density at each vertex. The vertex value corresponds to the cell value of the
    given array.
    
    Idea from https://fenicsproject.org/olddocs/dolfin/1.4.0/python/demo/documented/tensor-weighted-poisson/python/documentation.html
    https://fenicsproject.org/qa/2715/coordinates-u_nodal_values-using-numerical-source-function?show=2721#a2721
    
    •••••••••••••••••••••••••••••••
    •     •     •     •     •     •<== Pixel/Voxel
    •  ○--•--○--•--○--•--○--•--○  •
    •  |  • /|  • /|  • /|  • /|  •
    •  |  •/ |  •/ |  •/ |  •/ |<===== Mesh
    •••••••••••••••••••••••••••••••
    •  | /•  | /•  | /•  | /•  |  •
    •  |/ •  |/ •  |/ •  |/ •  |  •
    •  ○--•--○--•--○--•--○--•--○<===== Vertex
    •  |  • /|  • /|  • /|  • /|  •
    •  |  •/ |  •/ |  •/ |  •/ |  •
    •••••••••••••••••••••••••••••••
    •  | /•  | /•  | /•  | /•  |  •
    •  |/ •  |/ •  |/ •  |/ •  |  •
    •  ○--•--○--•--○--•--○--•--○  •
    •     •     •     •     •     •
    •••••••••••••••••••••••••••••••
    
    """
    
    # Generate a mesh given the array
    if array.ndim == 2:
        print("2-dimensional input")
        xn, yn = array.shape
        mesh = RectangleMesh(Point(0.5,0.5), Point(xn-0.5, yn-0.5), xn-1, yn-1, 'right')
    elif array.ndim == 3:
        print("3-dimensional input")
        xn, yn, zn = array.shape
        mesh = BoxMesh(Point(0.5,0.5,0.5), Point(xn-0.5, yn-0.5, zn-0.5), xn-1, yn-1, zn-1)
    else:
        raise NameError('Not suported dimension of array. Only 2D and 3D is supported.')
    
    # Define function space
    V = FunctionSpace(mesh, "Lagrange", 1)
    
    # Define density function
    mu = Function(V)
    
    vertex_values = np.zeros(mesh.num_vertices())

    if array.ndim == 2:
        for vertex in vertices(mesh):
            i = int(np.trunc(vertex.x(0)))
            j = int(np.trunc(vertex.x(1)))
            vertex_values[vertex.index()] = array[i,j].item()
    elif array.ndim == 3:
        for vertex in vertices(mesh):
            i = int(np.trunc(vertex.x(0)))
            j = int(np.trunc(vertex.x(1)))
            k = int(np.trunc(vertex.x(2)))
            vertex_values[vertex.index()] = array[i,j,k].item()

    mu.vector()[:] = vertex_values[dof_to_vertex_map(V)]
    
    return mesh, V, mu
