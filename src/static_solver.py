# -*- coding: iso-8859-15 -*-

# Static solver for moving mesh and image registration using FEniCS.
#
# References:
# [1]   http://dx.doi.org/10.1137/151006238
#
# Code by:
#       Alexander Sehlstrom
#       alexander.sehlstrom@chalmers.se
#       Department of Architecture and Civil Engineering
#       Chalmers University of Technology
#
#       Jenny Larsson
#       jslarsson1@sheffield.ac.uk
#       Department of Animal and Plant Sciences
#       University of Sheffield
from fenics import *
import narwhal as na

# Generate a folder for output
mydir = 'moving_mesh_registration_output/'
na.ensureoutputfolder(mydir)

# Read the images to arrays and make values into the span [0,1]
from scipy import misc
array0 = misc.imread('face_source.png', flatten=True, mode='F') / 255.0
array1 = misc.imread('face_target.png', flatten=True, mode='F') / 255.0

# Check dimensions of arrays and make mesh
if array0.shape == array1.shape:
    Lx, Ly = array0.shape
    mesh, V, W = torusmesh(Lx, Ly)
else:
    raise NameError('Dimension mismatch of provided image arrays.')

# Generate density functions
mu0 = na.arraytodensity(mesh, V, array0)
mu1 = na.arraytodensity(mesh, V, array1)

# Define variational problem
u = TrialFunction(W)
v = TestFunction(W)

f = mu0-mu1

a1 = dot(u,v)*dx 
a2 = 2*inner(grad(u), grad(v))*dx 
a3 = dot(div(grad(u)), div(grad(v)))*dx
a = a1 + a2 +a3 

L = - f*div(v)*dx

# Compute solution
u = Function(W)
solve(a == L, u)

# Save u, mu0 and mu1 to file
file = File(mydir+"solution.pvd")
file << u
file = File(mydir+"source.pvd")
file << mu0
file = File(mydir+"target.pvd")
file << mu1